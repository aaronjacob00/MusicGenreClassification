from sklearn.svm import NuSVC
import numpy as np
from sklearn import preprocessing
import pickle
import sklearn.metrics as metrics

# nu = 0.5

def safeLoad(filename):
	return pickle.load(open(filename, 'rb'))

def safeKeep(obj, filename):
	with open(filename, 'wb') as op:
		pickle.dump(obj.op, pickle.HIGHEST_PROTOCOL)

X = np.array(safeLoad('trainvec.pkl'))
Y = safeLoad('labels.pkl')
testX = np.array(safeLoad('testvec.pkl'))
testY = np.array(safeLoad('testlabel.pkl'))


clf = NuSVC(kernel = 'rbf', nu = 0.15)

X = preprocessing.scale(X)
testX = preprocessing.scale(testX)

clf.fit(X, Y)

predVal = clf.predict(testX)

pop = []
jazz = []
metal = []
classical = []
reggae = []

for i,val in enumerate(testY):
	if val == 'pop':
		pop.append([val,predVal[i]])
	elif val =='jazz':
		jazz.append([val,predVal[i]])
	elif val == 'metal':
		metal.append([val,predVal[i]])
	elif val == 'reggae':
		reggae.append([val,predVal[i]])
	else:
		classical.append([val,predVal[i]])

def split(data,num):
	pred  = []
	for i in data:
		pred.append(i[num])
	return pred

pop_accuracy = metrics.accuracy_score(split(pop,0),split(pop,1))
jazz_accuracy = metrics.accuracy_score(split(jazz,0),split(jazz,1))
metal_accuracy = metrics.accuracy_score(split(metal,0),split(metal,1))
classical_accuracy = metrics.accuracy_score(split(classical,0),split(classical,1))
reggae_accuracy = metrics.accuracy_score(split(reggae,0),split(reggae,1))

print "Pop accuracy:", pop_accuracy
print "Jazz accuracy:",jazz_accuracy
print "Metal accuracy:",metal_accuracy
print "Classical accuracy:",classical_accuracy
print "Reggae accuracy:",reggae_accuracy
print "Overall accuracy:",metrics.accuracy_score(testY,predVal)
# print metrics.f1_score(testY,predVal, average='macro')
