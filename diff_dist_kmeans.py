import scipy as sp
import numpy as np
import pickle
import random
import math
import scipy.spatial.distance

def safeLoad(filename):
	return pickle.load(open(filename, 'rb'))

def safeKeep(obj,filename):
	with open(filename, 'wb') as op:
		pickle.dump(obj,op,pickle.HIGHEST_PROTOCOL)

dictionary = {}
dictionary[0] = "braycurtis"
dictionary[1] = "canberra"
dictionary[2] = "chebyshev"
dictionary[3] = "cityblock"
dictionary[4] = "correlation"
dictionary[5] = "cosine"
dictionary[6] = "euclidian"
dictionary[7] = "hamming"
dictionary[8] = "mahalanobis"

class K_Means:

	def __init__(self,k=4,tolerance=0.0001,max_iterations=500,dist_type=0):
		self.k = k
		#when difference b/w old and new centroids is tolerance, we stop
		self.tolerance = tolerance
		self.max_iterations = max_iterations
		self.dist_type = dist_type

	def calc_distance(self,p,q):
		if self.dist_type == 0:
			return scipy.spatial.distance.braycurtis(p,q)
		elif self.dist_type == 1:
			return scipy.spatial.distance.canberra(p,q)
		elif self.dist_type == 2:
			return scipy.spatial.distance.chebyshev(p,q)
		elif self.dist_type == 3:
			return scipy.spatial.distance.cityblock(p,q)
		elif self.dist_type == 4:
			return scipy.spatial.distance.correlation(p,q)
		elif self.dist_type == 5:
			return scipy.spatial.distance.cosine(p,q)
		elif self.dist_type == 6:
			return scipy.spatial.distance.euclidean(p,q)
		elif self.dist_type == 7:
			return scipy.spatial.distance.hamming(p,q)
	

	def normalize(self,p):
		min_p = min(p)
		p = p - min_p
		sum_p = sum(p)
		p = p/sum_p
		#print sum(p)
		return p

	def KL_dist(self,p,q):
		p = self.normalize(p)
		q = self.normalize(q)
		#one_way = np.sum(np.where(p != 0,(p) * np.log10(p / q), 0))
		#other_way = np.sum(np.where(q != 0,(q) * np.log10(q / p), 0))
		
		#between p and q
		one_sum = 0
		for i in range(0,len(p)):
			if q[i] != 0:
				intm = (p[i]/q[i])
				#print intm
				if intm:
					one_sum += math.log(intm) * p[i]
			else:
				pass
		two_sum = 0
		for i in range(0,len(q)):
			if p[i] != 0:
				intm = (q[i]/p[i])
				#print intm
				if intm:
					one_sum += math.log(intm) * q[i]
			else:
				pass


		return two_sum + one_sum

	def fit(self,data):
		
		#initializing first k as centroids
		self.centroids = {}
		#hard coding centroid based on prior best results
		numbers = [159,37,86,280]
		nums_picked = []
		#print
		#print "initial centroids used : ",
		i = 0
		while True:
			if len(nums_picked) == 4:
				break
			else:
				num = random.randint(0,len(data)-1)
				if num not in nums_picked:
					nums_picked.append(num)
					#print num," ", 
					self.centroids[i] = data[num]
					i+=1
		
		#loop for each iteration
		for i in range(self.max_iterations):
			self.classifications = {}

			#lists for each cluster
			for j in range(self.k):
				self.classifications[j] = []

			#assigning each datapoint to a cluster
			for featureset in data:
				distances = [self.calc_distance(featureset,self.centroids[centroid]) for centroid in self.centroids]
                		classification = distances.index(min(distances))
                		self.classifications[classification].append(featureset)
	
			prev_centroids = self.centroids
			
			#recalculating centroids
			for classification in self.classifications:
				self.centroids[classification] = np.average(self.classifications[classification],axis=0)

			optimized = True
			
			#checking if not much change in centroids
			for c in self.centroids:
				old_centroid = prev_centroids[c]
				new_centroid = self.centroids[c]
				if np.sum((old_centroid - new_centroid)/old_centroid*100.0) > self.tolerance:
					optimized = False
				else:
					pass
			
			#break if optimized
			if optimized:
				break

	def predict(self,data):
		array = []
		for featureset in data:
			distances = [self.calc_distance(featureset,self.centroids[centroid]) for centroid in self.centroids]
			classification = distances.index(min(distances))
			array.append(classification)
		
		return array


if __name__ == '__main__':

	



	for iterr in range(0,8):
		best_acc = 0
		avg_acc = 0

		for m in range(0,100):

			#print "iter - ", m
		
			X =  np.array(safeLoad('trainvec.pkl'))
			Y =  np.array(safeLoad('labels.pkl'))
			testX = np.array(safeLoad('testvec.pkl'))
			testY = np.array(safeLoad('testlabel.pkl'))
			ans = np.linalg.norm(X[0]-X[1])
			clf = K_Means(dist_type=iterr)
			clf.fit(X)
			predictions_train = clf.predict(X) 
			predictions_test = clf.predict(testX)
			#list saying how many times a particular label(0,1,2,3)[based on index of the list] is predicted by kmeans when it is actually (classical,pop,metal,jazz)
			#this is all on training thing cz we are assigning the actual labels(classical ... ) to the (0,1,..) of kmeans based on majority
			list_classical = []
			list_pop = []
			list_jazz = []
			list_metal = []
			labels_used_up = []

			#initialising
			for i in range(0,4):
				list_classical.append(0)
				list_jazz.append(0)
				list_metal.append(0)
				list_pop.append(0)
				labels_used_up.append(0)

			#according to kmeans predictions our lists now have number of times a particular label (0,1,2,3) is predicted how many times it actually was (classical...)
			for i in range(0,len(predictions_train)):
				if Y[i] == 'classical':
					list_classical[int(predictions_train[i])] += 1
				elif Y[i] == 'jazz':
					list_jazz[int(predictions_train[i])] += 1
				elif Y[i] == 'pop':
					list_pop[int(predictions_train[i])] += 1
				else:
					list_metal[int(predictions_train[i])] += 1

			# print "classical", list_classical
			# print "jazz", list_jazz
			# print "pop", list_pop
			# print "metal", list_metal

			#dict of the map
			dict_labels = {}

			dict_labels['metal'] = -1
			dict_labels['pop'] = -1
			dict_labels['jazz'] = -1
			dict_labels['classical'] = -1

			max_metal = max(list_metal)
			max_metal_index = list_metal.index(max_metal)
			dict_labels['metal'] = max_metal_index
			labels_used_up[max_metal_index] = -1

			# print dict_labels
			# print labels_used_up

			#assigning map based on majority since observation shows pop and metal do best job assigning for them first

			while dict_labels['pop'] == -1:
				max_pop = max(list_pop)
				max_pop_index = list_pop.index(max_pop)
				if labels_used_up[max_pop_index] == -1:
					list_pop[max_pop_index] = -1
				else:
					dict_labels['pop'] = max_pop_index
					labels_used_up[max_pop_index] = -1

			while dict_labels['jazz'] == -1:
				max_jazz = max(list_jazz)
				max_jazz_index = list_jazz.index(max_jazz)
				if labels_used_up[max_jazz_index] == -1:
					list_jazz[max_jazz_index] = -1
				else:
					dict_labels['jazz'] = max_jazz_index
					labels_used_up[max_jazz_index] = -1

			while dict_labels['classical'] == -1:
				#print "here"
				max_classical = max(list_classical)
				max_classical_index = list_classical.index(max_classical)
				#print max_classical_index
				if labels_used_up[max_classical_index] == -1:
					list_classical[max_classical_index] = -1
				else:
					dict_labels['classical'] = max_classical_index
					labels_used_up[max_classical_index] = -1

			#print dict_labels

			#calculating accuracy on test data

			num_correct = 0
			num_wrong = 0

			for i in range(0,len(predictions_test)):
			
				if predictions_test[i] == dict_labels[testY[i]]:
					num_correct += 1
			
				else:
					num_wrong += 1


			accuracy = num_correct/float(num_correct + num_wrong)
			if accuracy > best_acc:
				best_acc = accuracy
			avg_acc += accuracy
			#print m, " : accuracy - ", accuracy


		print "DISTANCE MEASURE USED : ", dictionary[iterr]
		print "BEST ACCURACY : ", best_acc
		print "AVERAGE ACCURACY : ", avg_acc/100
		print