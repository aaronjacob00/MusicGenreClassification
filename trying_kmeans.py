import pylab as pl
import scipy as sp
import pickle
import numpy as np
from sklearn import cluster, datasets



def safeLoad(filename):
	return pickle.load(open(filename, 'rb'))

def safeKeep(obj,filename):
	with open(filename, 'wb') as op:
		pickle.dump(obj,op,pickle.HIGHEST_PROTOCOL)

X =  np.array(safeLoad('trainvec.pkl'))
Y =  np.array(safeLoad('labels.pkl'))
testX = np.array(safeLoad('testvec.pkl'))
testY = np.array(safeLoad('testlabel.pkl'))

k_means = cluster.KMeans(n_clusters=4)
k_means.fit(X)

predictions_train = k_means.predict(X) 
predictions_test = k_means.predict(testX)


#using predictions_train for finding cluster labels
#after doing kmeans clustering assigning mapping between actual labels and the 0,1,2,3 based on majority


#list saying how many times a particular label(0,1,2,3)[based on index of the list] is predicted by kmeans when it is actually (classical,pop,metal,jazz)
#this is all on training thing cz we are assigning the actual labels(classical ... ) to the (0,1,..) of kmeans based on majority
list_classical = []
list_pop = []
list_jazz = []
list_metal = []
labels_used_up = []

#initialising
for i in range(0,4):
	list_classical.append(0)
	list_jazz.append(0)
	list_metal.append(0)
	list_pop.append(0)
	labels_used_up.append(0)

#according to kmeans predictions our lists now have number of times a particular label (0,1,2,3) is predicted how many times it actually was (classical...)
for i in range(0,len(predictions_train)):
	if Y[i] == 'classical':
		list_classical[int(predictions_train[i])] += 1
	elif Y[i] == 'jazz':
		list_jazz[int(predictions_train[i])] += 1
	elif Y[i] == 'pop':
		list_pop[int(predictions_train[i])] += 1
	else:
		list_metal[int(predictions_train[i])] += 1

#dict of the map
dict_labels = {}

dict_labels['metal'] = -1
dict_labels['pop'] = -1
dict_labels['jazz'] = -1
dict_labels['classical'] = -1

max_metal = max(list_metal)
max_metal_index = list_metal.index(max_metal)
dict_labels['metal'] = max_metal_index
labels_used_up[max_metal_index] = -1

#assigning map based on majority since observation shows pop and metal do best job assigning for them first

while dict_labels['pop'] == -1:
	max_pop = max(list_pop)
	max_pop_index = list_pop.index(max_pop)
	if labels_used_up[max_pop_index] == -1:
		list_pop[max_pop_index] = -1
	else:
		dict_labels['pop'] = max_pop_index

while dict_labels['jazz'] == -1:
	max_jazz = max(list_jazz)
	max_jazz_index = list_jazz.index(max_jazz)
	if labels_used_up[max_jazz_index] == -1:
		list_pop[max_jazz_index] = -1
	else:
		dict_labels['jazz'] = max_jazz_index

while dict_labels['classical'] == -1:
	max_classical = max(list_classical)
	max_classical_index = list_classical.index(max_classical)
	if labels_used_up[max_classical_index] == -1:
		list_pop[max_classical_index] = -1
	else:
		dict_labels['classical'] = max_classical_index

print dict_labels

#calculating accuracy on test data

num_correct = 0
num_wrong = 0

for i in range(0,len(predictions_test)):
	
	if predictions_test[i] == dict_labels[testY[i]]:
		num_correct += 1
	
	else:
		num_wrong += 1


accuracy = num_correct/float(num_correct + num_wrong)
print "accuracy - ", accuracy
