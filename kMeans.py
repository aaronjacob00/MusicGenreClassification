import numpy as np
from numpy import linalg
import matplotlib.pyplot as plt
import pylab
from sklearn.cluster import KMeans
import sklearn.metrics as metrics
from random import randint
# from sklearn import datasets
import pickle
import math
import time


def safeLoad(filename):
	return pickle.load(open(filename, 'rb'))
#
X = np.array(safeLoad('trainvec.pkl'))
Y =  safeLoad('labels.pkl')
testX = np.array(safeLoad('testvec.pkl'))
testY =  safeLoad('testlabel.pkl')
#
# def klmetrix(p,q):
# 	global i
# 	p = abs(min(p)) + p
# 	q = abs(min(q)) + q
# 	pnorm = p/max(p)
# 	qnorm = q/max(q)
# 	# print sum(pnorm),sum(qnorm)
# 	h = np.finfo(float).eps
# 	kl1 = pnorm * np.log2((pnorm + h)/(qnorm + h))
# 	kl2 = qnorm * np.log2((qnorm + h)/(pnorm + h))
# 	dist = sum(kl1 + kl2)
# 	print dist,i
# 	i += 1
# 	return dist
#
# # # kmeans = KMeans(init='k-means++', n_clusters=10, n_init=10)
# # kmeans = KMeans(n_clusters=10, n_init=1)
# # kmeans.fit(X)
# # k_means_cluster_centers = np.sort(kmeans.cluster_centers_, axis=0)
# # k_means_labels = metrics.pairwise_distances_argmin(X, k_means_cluster_centers)
#
# def cov(arr):
# 	return np.cov(arr)
#
# def kl_divergence(p,q):
# 	p = abs(min(p)) + p
# 	q = abs(min(q)) + q
# 	pnorm = p/max(p)
# 	qnorm = q/max(q)
# 	# print sum(pnorm),sum(qnorm)
# 	h = np.finfo(float).eps

#
#
# class kmeans:
# 	def __init__(self,trainfeatures,trainlabels,testfeatures,testlabels):
# 		self.tf = trainfeatures
# 		self.tl = trainlabels
# 		self.sf = testfeatures
# 		self.sl = testlabels
#
# 	#n is number of centroids
# 	def random_centroids(self,n):


#
# # kmeans = KMeans(init='k-means++', n_clusters=10, n_init=10)
# kmeans = KMeans(n_clusters=4, n_init=1)
# kmeans.fit(X)
# print kmeans.cluster_centers_
# k_means_cluster_centers = np.sort(kmeans.cluster_centers_, axis=0)
# k_means_labels = metrics.pairwise_distances_argmin(X, k_means_cluster_centers)
#
#
# predictions = kmeans.predict(testX)
#
# print predictions
# print k_means_labels.shape
# predictions = kmeans.predict(X[0:2])

# print predictions
# print k_means_labels.shape
n = 15
g = 4

songs = []
actualGenre = ['pop', 'jazz', 'metal', 'classical']

class Song:
	def __init__(self,features,cluster,genre):
		self.features = features
		self.cluster = cluster
		self.genre = genre
		self.genreEnum = actualGenre.index(self.genre)

centroid_array = []

def setup(x):
	for i in range(len(x)):
		songs.append(Song(x[i],0,Y[i]))

song_n = 15
def KL_divergence(p, q):
	print q.shape,p.shape
	# Skipped the log(det(sample.cov_inv)/det(test.sample.cov_inv)) coz core purpose is to calculate distance not
	p = np.transpose(np.reshape(p, (1511,15)))
	q = np.transpose(np.reshape(q, (1511,15)))
	dist = -song_n
	dist += np.trace(np.linalg.inv(np.cov(p)).dot(np.cov(q)))
	# print(dist)
	temp = (np.mean(p,axis=1) - np.mean(q,axis=1))
	dist +=  temp.T.dot(np.linalg.inv(np.cov(p))).dot(temp)
	return abs(dist/2)


def kMeans():
	global n
	meanDiff = np.zeros(g)
	ansMat = np.zeros((4,4))

	for loop in range(n):
		for j in range(g):
			random_index = randint(j * len(X)/g,(j+1) * len(X)/g)
			centroid_array.append(Song(X[random_index],j,Y[random_index]))

		# newMean = [np.array([]),np.array([]),np.array([]),np.array([])]
		# count = [0,0,0,0]
		# meanDiff =
		tempcentroidArray = centroid_array

		# not_converged = True
		loopCheck = 0
		while loopCheck < 100:
			print "Iteration no:", loopCheck
			loopCheck += 1
			for song in songs:
				dist_array = [0,0,0,0]
				for i in range(g):
					dist_array[i] = KL_divergence(song.features,centroid_array[i].features) + KL_divergence(centroid_array[i].features,song.features)
				song.cluster = dist_array.index(min(dist_array))
				tempcentroidArray[i].features = 
				# newMean[song.cluster] = np.append(newMean[song.cluster],song.features)
				# count[song.cluster] += 1
				print song
			# for i in range(len(g)):
			# 	meanDiff[i] = centroid_array[i]
			# 	centroid_array[i] *= 0

			# for i in range(g):
			# 	newMean[i] /= count[i]
			# 	centroid_array[i] = Song(newMean[i],i,Y[i])

		for song in songs:

			ansMat[song.genreEnum][song.cluster]
	print ansMat/n

setup(X)
kMeans()
